public class SyncMix extends Helper {

	public synchronized void synchronizedMethod() {
		log("BeginSync");
		sleep(1000);
		log("EndSync");
	}

	public void unsynchronizedMethod() {
		log("Unsynchronized");
	}

	/*
	 * shows that synchronized blocks do not stop unsynchronized methods/blocks from
	 * executing
	 */
	public static void main(String[] args) throws InterruptedException {

		SyncMix mix = new SyncMix();

		Thread a = new Thread(mix::synchronizedMethod);
		Thread b = new Thread(mix::unsynchronizedMethod);
		Thread c = new Thread(mix::synchronizedMethod);

		a.start();
		b.start();
		c.start();

		a.join();
		b.join();
		c.join();
	}

}