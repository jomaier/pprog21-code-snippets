public class Helper {

	static void log(String msg) {
		System.out.printf("%s>\t%s\n", Thread.currentThread().getName(), msg);
	}

	static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
