
public class WaitNotify extends Helper {

	/*
	 * shows how wait() and notify() interact with each other
	 */
	public static void main(String[] args) throws InterruptedException {
		// lock object which we call wait and notify on
		Object lock = new Object();

		var notifier = new Thread(new Notifier(lock));
		notifier.setName("Notifier");

		var waiters = new Thread[2];
		for (int i = 0; i < waiters.length; i++) {
			waiters[i] = new Thread(new Waiter(lock));
			waiters[i].setName("Waiter-" + i);
		}

		for (var waiter : waiters) {
			waiter.start();
		}

		sleep(1000);

		notifier.start();
	}
}


/*
 * calls .notify on an object.
 */
class Notifier extends Helper implements Runnable {
	final Object lock;

	public Notifier(Object lock) {
		this.lock = lock;
	}

	public void run() {
		synchronized (lock) {
			log("Begin");

			// Artificial waiting
			sleep(1000);

			log("PreNotify");
			lock.notify();
			log("PostNotify");

			// Artificial waiting
			sleep(1000);

			log("End");
		}
	}
}
/*
 * calls .wait on the lock object
 */
class Waiter extends Helper implements Runnable {
	final Object lock;

	public Waiter(Object lock) {
		this.lock = lock;
	}

	public void run() {
		synchronized (lock) {
			log("Begin");
			while (true) {
				try {

					log("PreWait");
					lock.wait();
					log("PostWait");

					// Successfully waited
					break;

				} catch (InterruptedException e) {
					
					// Duh, we got interrupted
					// Let's wait again

					log("Interrupted");
					sleep(1000);

				}
			}
			log("End");
		}
	}
}
