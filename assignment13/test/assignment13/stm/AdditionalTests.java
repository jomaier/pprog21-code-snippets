package assignment13.stm;

import java.util.concurrent.atomic.AtomicBoolean;
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class AdditionalTests {

	/*
	 * ensures that each value that is put onto the buffer gets taken away / read
	 * exactly once.
	 */
	@Test
	public void singleRead() throws InterruptedException {
		int REPS = 10_000;
		int CAPACITY = 50;
		int CONSUMERS = 4;

		var seen = new AtomicBoolean[REPS];
		for (int i = 0; i < REPS; i++) {
			seen[i] = new AtomicBoolean(false);
		}

		var buffer = new CircularBufferSTM<Integer>(CAPACITY);

		// the producer thread puts each value between 0 and REPS once into the buffer
		var producer = new Thread(() -> {
			for (int i = 0; i < REPS; i++) {
				buffer.put(i);
			}
		});

		var consumers = new Thread[CONSUMERS];

		// the consumers take away REPS/CONSUMERS values each, and mark them as seen
		for (int t = 0; t < CONSUMERS; t++) {
			consumers[t] = new Thread(() -> {
				for (int i = 0; i < REPS / CONSUMERS; i++) {
					int item = buffer.take();
					seen[item].getAndSet(true);
				}
			});
			consumers[t].start();
		}

		producer.start();
		for (int t = 0; t < CONSUMERS; t++) {
			consumers[t].join();
		}
		producer.join();

		// each value must have been seen once:
		// this also ensures a value hasn't been read multiple times,
		// as then another value must have been ignored.
		for (int i = 0; i < REPS; i++) {
			assertTrue("value " + i + " was never read.", seen[i].get());
		}

	}

}
